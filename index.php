<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="navbar">
            <div class="navbar__wrapper">
            <table>
                <tr>
                    <td id="navbar__logo">
                        <?php
                        $icon_directory = 'icon';      
                            
                        echo '<img src="'.$icon_directory.'/logo.svg">'
 
                        ?>
                    </td>  
                    <td id="navbar__menu" width="80%">
                        <div class="menu-test-container"><ul id="menu-test" class="menu"><li id="menu-item-27" class="menu-list menu-item menu-item-type-custom menu-item-object-custom menu-item-27"><a href="#">ABOUT</a></li>
                            <li id="menu-item-28" class="menu-list menu-item menu-item-type-custom menu-item-object-custom menu-item-28"><a href="#">MENU</a></li>
                            <li id="menu-item-29" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a href="#">CONTACT</a></li>
                            <li id="menu-item-30" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a href="#">BLOG</a></li>
                            <li id="menu-item-31" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a href="#MOODS">MOODS</a></li>
                            </ul>
                        </div>                    </td>
                    <td id="navbar__search">
                        <i class="fa fa-search fa-2x fa__color--grey" aria-hidden="true"></i>
                    </td>
                </tr>
            </table>
            </div>
        </div>
        <div class="header__bg">
            <div id="header__title">
                Life Begins After Coffee.
            </div>
            <div id="header__button">
                <center>
                    <button id="btn__menu" class="content__button" style="margin-top:10px">VIEW MENU</button>
                </center>
            </div>
        </div>
            <!-- #header END -->
<div id="container">
    <div class="content--1">
        <div class="content__text">
            <div id="content__text--menu" class="content__text--title">
                What would you like to have?
            </div>
            <div class="content__text--desc">
                Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo
                variety, as, siphon, ristretto, iced brewed and acerbic affogato grind
            </div>
        </div>
        <div class="content__loop--img">
            <table>
                <tr>
                    <?php
                    $url = 'json/json.php';   ;
                    $data = file_get_contents($url);
                    $images = json_decode($data, true);
            
                    foreach ($images as $image) {
                        
                        $file_name = $image['file_name'];
                        $desc = $image['desc'];
                        
                        echo '<td>
                        <div class="'.$file_name.'">
                            <div class="gallery__title">
                                '.$desc.'
                            </div>
                        </div>
                        </td>';
                    }
                    ?>
                </tr>
            </table>
        </div>
        <div class="content__text">
            <div class="content__text--title">
                Extraction instant that variety
                white robusta strong
            </div>
            <div class="content__text--desc">
                Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo
                variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon,
                percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half.
            </div>
        </div>
        <center>
            <button id="btn__contact" class="content__button">CONTACT US</button>
        </center>
    </div>
    <div class="content--2">
        <div class="content__text">
            <div class="content__text--title">
                Health Benefits of Coffee
            </div>
        </div>
        <div class="content__icon">
            <table class="content__icon--wrapper">
                <?php
                $icon_directory = 'icon';      
                $icon = array("battery-full" => "BOOST ENERGY LEVEL",
                              "sun" => "REDUCE DEPRESSION",
                              "weight" => "AID IN WEIGHT LOSS");
 
                ?>
                <tr>
                <?php 
                foreach ($icon as $key => $value){
                    
                    $icon_name = $key.'.svg';
                    
                    echo '<td class="content__icon--img">
                    <img src='.$icon_directory."/".$icon_name.'>
                    </td>';
                    
                }
                ?>
                </tr>
                <tr>
                <?php 
                foreach ($icon as $key => $value){
                    
                    echo '<td class="content__icon--desc">
                    '.$value.'
                    </td>';
                    
                }
                ?>
                </tr>
        </table>
        </div>
    </div>
    <div class="content--3">
        <div class="content__multiside">
            <table>
                <tr>
                    <td class="content__multiside--img">
                        <?php
                        $img_directory = 'images';      
                            
                        echo '<img src="'.$img_directory.'/blog.jpg">'
 
                        ?>
                    </td>
                    <td class="content__multiside--separator">
                    </td>
                    <td class="content__multiside--text">
                        <div class="content__multiside--titlesm">BLOG</div>
                        <div class="content__multiside--titlelg">Qui espresso, grounds to go</div>
                        <div class="content__multiside--date">December 12, 2019 | Espresso</div>
                        <div class="content__multiside--desc">Skinny caffeine aged variety filter saucer redeye, sugar
                            sit steamed eu extraction organic. Beans, crema half
                            and half fair trade carajillo in a variety dripper doppio
                            pumpkin spice cup lungo, doppio, est trifecta breve and,
                            rich, extraction robusta a eu instant. Body sugar
                            steamed, aftertaste, decaffeinated coffee fair trade sit,
                            white shop fair trade galão, dark crema breve
                            frappuccino iced strong siphon trifecta in a at viennese.
                        </div>
                        <div class="content__multiside--readmore">READ MORE <span class="content__multiside--readmore-icon"><i class="fa fa-arrow-right fa-xs" aria-hidden="true"></i></span></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div id="footer">
    <div class="footer__wrapper">
            <table>
                <tr>
                    <td class="footer__img">
                        <?php
                        $icon_directory = 'icon';      
                            
                        echo '<img src="'.$icon_directory.'/logo.svg" class="grayscale">'
 
                        ?>
                    </td>
                    <td class="footer__contact">
                        <div class="footer__contact--desc">
                            2800 S White Mountain Rd | Show Low AZ 85901<br>
                            (928) 537-1425 | info@grinder-coffee.com
                        </div>
                        <div class="footer__contact--icon" align="left">
                            <br>
                            <i class="fa fa-instagram fa-3x fa__color--grey" aria-hidden="true"></i> &nbsp; &nbsp; &nbsp; &nbsp;
                            <i class="fa fa-facebook fa-3x fa__color--grey" aria-hidden="true"></i>
                        </div>
                    </td>
                    <td class="footer__newsletter">
                        <div class="footer__newsletter--title">
                            NEWSLETTER
                        </div>
                        <div class="footer__newsletter--form">
                            <form class="form-inline" action="/action_page.php">
                                <div class="form-text">YOUR EMAIL ADDRESS</div>
                                <button type="submit">SUBSCRIBE</button>
                            </form>
                        </div>
                </tr>
            </table>
        </div>
</div>
<!-- #footer END --><br><br><!-- #Container END -->
<script>

var position = $(window).scrollTop(); 

// should start at 0

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    
    position = scroll;
    
    if(position > 200) {
        $("#navbar").addClass("navbar__background");
    } else {
        $("#navbar").removeClass("navbar__background");
       
    }
    
    console.log(position);
});
    
    $("#btn__menu").click(function() {
    $('html, body').animate({
        scrollTop: $("#content__text--menu").offset().top
    }, 2000);
});
</script>
</body>
</html>